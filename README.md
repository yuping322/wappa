# wappa
Compiler for a toy programming language

## What does it do?

Basic imperative programming, with primitive types.

```
fun sum(a: Int, b: Int) => Int where
    return a + b;
end

fun eq(a: Double, b: Double) => Bool where
    if a == b where
        return True;
     end
        return False;
end

fun neq(a: Double, b: Double) => Bool where
    return a != b;
end
```

While unused in the final language, there is also a type checker built including:
- Classes with multiple inheritance
  - Linearizing the inheritance hierarchy
- Union and intersection types
  - Optimizing type expressions

## Libraries Used

### ANTLR v4

Used for generating the lexer, parser and vistor classes.

### llvmlite

Used for code generation, builds LLVM IR and then compiles it into an binary.

## What now?

Dropped it as I caught the functional programming bug, as this was initially intended to be object-oriented.
Also, was looking for a reason to learn Rust, so started a new project using it, [Seta](https://github.com/ZippeyKeys12/seta)



fengzhi
curl -O https://www.antlr.org/download/antlr-4.10.1-complete.jar
java -jar antlr-4.10.1-complete.jar -Dlanguage=Python3 gen/* -visitor

pyenv install