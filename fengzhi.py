import llvmlite.binding as llvm
from antlr4 import CommonTokenStream, FileStream
from ctypes import CFUNCTYPE, c_double, c_int, c_bool

from compiler import Wappa, WappaLexer, WappaVisitor

text = FileStream("overview.txt")
lexer = WappaLexer(text)
tokens = CommonTokenStream(lexer)
parser = Wappa(tokens)
parser.buildParseTrees = True

tree = parser.compilationUnit()
visitor = WappaVisitor()
module = visitor.visit(tree)

with open("ex/o.ll", "w") as f:
    f.write(module)